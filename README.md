# Resources

Some things I personally use or make for myself might benefit from others' ideas, or be useful to others. So I'm putting them here.